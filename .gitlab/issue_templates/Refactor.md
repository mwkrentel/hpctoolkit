<!-- Summarize your proposed refactor. In general terms, what do you want changed? -->

### Current Issues / Rationale

<!-- Please detail any rationale for performing this refactor here. Why would it help develop or improve HPCToolkit moving forward? What development difficulties have you experienced that need to be solved? Link any related bugs or feature requests here. -->

### Proposed Solution

<!-- If you have an idea for a solution, detail it here. How do you want the internal APIs to behave? What language does it need to be written in? Link any other blocking changes here. -->

#### Affected Code Paths

<!-- Please detail any code paths that would need to significantly change because of this refactor as a (possibly nested) bullet list. If possible, provide links to the source files that are most affected by this change. -->

### Additional Information

<!-- Please provide any additional information that might spark discussion. Link any external sources or inspiration here, if possible. -->

<!-- Do not remove the following line. -->

/label ~"type::review"
