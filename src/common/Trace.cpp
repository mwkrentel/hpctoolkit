// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*-

//************************** System Include Files ***************************

//*************************** User Include Files ****************************

#include "Trace.hpp"

//*************************** Forward Declarations **************************

//***************************************************************************

int trace = 0;
