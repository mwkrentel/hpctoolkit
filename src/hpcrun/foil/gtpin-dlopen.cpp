// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

#include "gtpin-private.h"

constexpr const hpcrun_foil_appdispatch_gtpin hpcrun_dispatch_gtpin;
