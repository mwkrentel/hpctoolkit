// SPDX-FileCopyrightText: 2019-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef _HPCTOOLKIT_DEVICE_INITIALIZERS_H_
#define _HPCTOOLKIT_DEVICE_INITIALIZERS_H_

extern void hpcrun_initializer_init();

#endif
