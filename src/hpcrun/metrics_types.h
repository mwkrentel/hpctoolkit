// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef METRICS_TYPES_H
#define METRICS_TYPES_H
/* indices into a treenode's `metrics' array */
#define CALLS_METRIC 1
#endif
