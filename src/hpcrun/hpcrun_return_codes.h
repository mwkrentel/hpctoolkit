// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef CSPROF_MISC_FN_STAT_H
#define CSPROF_MISC_FN_STAT_H
#define HPCRUN_OK      1
#define HPCRUN_ERR    -1
#endif
