// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef SAMPLE_SOURCE_NONE_H
#define SAMPLE_SOURCE_NONE_H

extern void hpcrun_process_sample_source_none(void);

#endif // SAMPLE_SOURCE_NONE_H
